package isima.f2.TP3.Guilbert;

import org.springframework.stereotype.Component;


@Component

public class FeeService {

    public Double computeFee(final Long nbKm) throws Exception {
        double remboursementCourt = 1.5;
        double remboursementMoyen = 0.4;
        double remboursementLong = 0.55;
        double remboursementTresLong = 6.81;
        double res;

        if(nbKm < 0)
        {
            throw new Exception();
        }


        if(nbKm>60) res = remboursementTresLong * ((nbKm-60)/20 + 1);
        else if(nbKm>40) res = nbKm * remboursementLong;
        else if(nbKm>10) res = nbKm * remboursementMoyen;
        else res = remboursementCourt * nbKm;

        return Math.round(res * 100.0) / 100.0;
    }

    public Double computeFee(final Double nbKm) throws Exception {
        double res=0;
        /*(nbKm < 0)
        {
            throw new Exception();
        }*/
        try {
            res = computeFee(nbKm.longValue());
        }
        catch (Exception e)
        {
            throw new Exception();
        }
        return res;
    }
}
