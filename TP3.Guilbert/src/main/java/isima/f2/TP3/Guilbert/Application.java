package isima.f2.TP3.Guilbert;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
public class Application {

	private static final Logger logger = Logger.getLogger(String.valueOf(DemoApplication.class));
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		logger.setLevel(Level.WARNING);
		logger.warning("Hello World");
		logger.warning(StringUtils.reverse("Clément Guilbert"));
		/*System.out.print("For ");
		System.out.print(200L);
		System.out.print("km : ");
		System.out.println(" Fee is : ");
		FeeService feeService = new FeeService();

		try {
			System.out.print(feeService.computeFee(200L));
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Test du ci-yml");*/
	}

	private static class DemoApplication {
	}
}
