package isima.f2.TP3.Guilbert;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertThrows;


@SpringBootTest
class ApplicationTests {

	@Autowired
	FeeService feeService;

	@Test
	void contextLoads() {

	}

	@Test
	void testLessThan10km() throws Exception {
		Assert.assertEquals((Double) 1.5D, feeService.computeFee(1L));
	}

	@Test
	void test0km() throws Exception {
		Assert.assertEquals((Double) 0D, feeService.computeFee(0L));
	}

	@Test
	void test60km() throws Exception {
		Assert.assertEquals((Double) 33D, feeService.computeFee(60L));
	}

	@Test
	void testLessThan40km() throws Exception {
		Assert.assertEquals((Double) 12D, feeService.computeFee(30L));
	}

	@Test
	void testLessThan60km() throws Exception {
		Assert.assertEquals((Double) 27.5D, feeService.computeFee(50L));
	}

	@Test
	void testMoreThan100km() throws Exception {
		Assert.assertEquals((Double) 54.48D, feeService.computeFee(200L));
	}

	@Test
	public void testNeg() throws Exception {
		Exception exception = assertThrows(Exception.class, () -> {
			feeService.computeFee(-1L);
		});
	}

	//-10 Km, 0 Km, 0.1 Km, 10.1 Km, 17.123 Km, 39.5 Km, 61 Km, 81 Km, 99 Km

	@Test
	public void testNeg2() throws Exception {
		Exception exception = assertThrows(Exception.class, () -> {
			feeService.computeFee(-10L);
		});
	}

	@Test
	public void testNegDouble() throws Exception {
		Exception exception = assertThrows(Exception.class, () -> {
			feeService.computeFee(-10.5D);
		});
	}

	@Test
	void test0() throws Exception {
		Assert.assertEquals((Double) 0D, feeService.computeFee(0L));
	}
	@Test
	void test01() throws Exception {
		Assert.assertEquals((Double) 0D, feeService.computeFee(0.1D));
	}
	@Test
	void test101() throws Exception {
		Assert.assertEquals((Double) 15D, feeService.computeFee(10.1D));
	}
	@Test
	void test395() throws Exception {
		Assert.assertEquals((Double) 15.6D, feeService.computeFee(39.5D));
	}
	@Test
	void test17123() throws Exception {
		Assert.assertEquals((Double) 6.8D, feeService.computeFee(17.123D));
	}

	@Test
	void test61() throws Exception {
		Assert.assertEquals((Double) 6.81D, feeService.computeFee(61L));
	}
	@Test
	void test81() throws Exception {
		Assert.assertEquals((Double) 13.62D, feeService.computeFee(81L));
	}
	@Test
	void test99() throws Exception {
		Assert.assertEquals((Double) 13.62D, feeService.computeFee(99L));
	}


}
